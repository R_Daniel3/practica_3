Práctica 3 Tecnologías para el Desarrollo de Software

Daniel Alderete Cillero

Guillermo Pastor Díez

El proyecto responde a un arquetipo básico de maven. Este a su vez descansa en 
un script ant, que se encarga de llamar a los diferentes goals del proyecto
maven para realizar los diferentes objetivos.

Los objetivos son:
a) compilar: garantiza la obtención de todas las dependencias y genera los
   .class a partir de los fuentes (depende de limpiar). Este objetivo es el 
   default.
   
b) ejecutar: incluye la ejecución de c), d) y e).

c) ejecutarTestsTDD: caja negra, test unitarios de cada método de cada clase.

d) ejecutarTestEnAislamiento: pruebas de caja blanca en aislamiento.

e) ejecutarPruebasSecuencia: caja negra, pruebas de secuencia.

f) obtenerInformeCobertura: informe sobre la ratio code to test además de 
   cobertura de sentencia, de rama, decisión/condición obtenidos con el total 
   de todos los tests realizados.
   
g) documentar: genera el javadoc del proyecto.

h) limpiar

El proyecto responde al arquetipo de maven "maven-archetype-quickstart"