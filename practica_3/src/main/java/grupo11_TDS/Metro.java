/**
 * 
 * @author Daniel Alderete
 * @author Guillermo Pastor
 *
 */
package grupo11_TDS;

import java.util.ArrayList;

/**
 * Clase que representa al metro. Tiene líneas y estaciones.
 *
 * @see <a
 *      href="https://docs.oracle.com/javase/7/docs/api/java/util/ArrayList.html">
 *      ArrayList </a>
 * @see Estacion
 * @see Linea
 * @since 1.0
 * @version 1.0
 */
public class Metro {

	/**
	 * Array con las líneas que forman el metro.
	 */
	private ArrayList<Linea> metro;

	/**
	 * Constructor, debe conocer las líneas que forman el metro y debe tener al
	 * menos dos líneas. Lanza una serie de excepciones si no se cumplen ciertas
	 * condiciones.
	 * 
	 * @param metro
	 *            Un <code>ArrayList</code> de <code>Linea</code> que forma el
	 *            metro.
	 * 
	 * @throws IllegalArgumentException
	 *             Si el <code>ArrayList</code> de <code>Linea</code> está vacío
	 *             o es nulo, o tiene líneas iguales o tiene menos de dos
	 *             líneas.
	 * @since 1.0
	 * @version 1.0
	 */
	public Metro(ArrayList<Linea> metro) {
		if (metro == null)
			throw new IllegalArgumentException("Metro nulo");
		if (metro.isEmpty())
			throw new IllegalArgumentException("Metro vacío");
		if (metro.size() < 2)
			throw new IllegalArgumentException("Metro con menos de dos líneas");
		if (!compruebaUnicidadLineas(metro))
			throw new IllegalArgumentException("Metro con líneas iguales");
		if (!compruebaUnicidadEstaciones(metro))
			throw new IllegalArgumentException("Metro con estaciones iguales");
		this.metro = metro;
	}

	/**
	 * Método que establece las líneas que forman el metro. Lanza una serie de
	 * excepciones si no se cumplen ciertas condiciones.
	 * 
	 * @param metro
	 *            Un <code>ArrayList</code> de <code>Linea</code> para
	 *            establecer como metro.
	 * @throws IllegalArgumentException
	 *             Si el metro es nulo, vacío o tiene menos de dos líneas.
	 * @since 1.0
	 * @version 1.0
	 */
	public void setMetro(ArrayList<Linea> metro) {
		if (metro == null)
			throw new IllegalArgumentException("Metro nulo");
		if (metro.isEmpty())
			throw new IllegalArgumentException("Metro vacío");
		if (metro.size() < 2)
			throw new IllegalArgumentException("Metro con menos de dos líneas");
		this.metro = metro;

	}

	/**
	 * Método que devuelve las líneas que forman el metro.
	 * 
	 * @return metro Un <code>ArrayList</code> de <code>Linea</code> con las
	 *         líneas que forman el metro.
	 *         
	 * @throws  IllegalStateException
	 *         	   Si el metro no tiene lineas
	 * @since 1.0
	 * @version 1.0
	 */
	public ArrayList<Linea> getLineas() {
		if(this.metro.isEmpty())
			throw new IllegalStateException("El metro no tiene lineas");
		return metro;
	}

	/**
	 * Método que obtiene las líneas que pasan por una estación. Lanza una serie
	 * de excepciones si no se cumplen ciertas condiciones.
	 * 
	 * @param estacion
	 *            La <code>Estacion</code> por donde se desea conocer que líneas
	 *            pasan.
	 * @return lineas Un <code>ArrayList</code> de <code>Linea</code> con las
	 *         líneas que pasan por esa estación.
	 * @throws IllegalArgumentException
	 *             Si la estación es nula.
	 *             
	 * @throws IllegalStateException
	 *         	   Si el metro no tiene lineas
	 * @since 1.0
	 * @version 1.0
	 */
	public ArrayList<Linea> getLineasPorEstacion(Estacion estacion) {
		if (estacion == null)
			throw new IllegalArgumentException("Estación nula");
		if(this.getLineas().isEmpty())
			throw new IllegalStateException("El metro no tiene lineas");
		ArrayList<Linea> lineas = new ArrayList<Linea>();
		for (int i = 0; i < metro.size(); i++) {
			if (metro.get(i).hasEstacion(estacion))
				lineas.add(metro.get(i));
		}
		return lineas;
	}

	/**
	 * Método que devuelve una línea a partir del nombre. Si no se encuentra la
	 * línea devuelve <code>null</code>. Lanza una serie de excepciones si no se
	 * cumplen ciertas condiciones.
	 * 
	 * @param nombre
	 *            El nombre de la <code>Linea</code>.
	 * @return linea Una <code>Linea</code> que responde a ese nombre.
	 * @throws IllegalArgumentException
	 *             Si el nombre es nulo o está vacío.
	 *@throws  IllegalStateException
	 *         	   Si el metro no tiene lineas.
	 * @since 1.0
	 * @version 1.0
	 */
	public Linea getLineaNombre(String nombre) {
		if (nombre == null)
			throw new IllegalArgumentException("Nombre nulo");
		if (nombre.isEmpty())
			throw new IllegalArgumentException("Nombre vacío");
		if(this.getLineas().isEmpty())
			throw new IllegalStateException("El metro no tiene lineas");
		Linea linea = null;
		for (int i = 0; i < metro.size(); i++) {
			if (metro.get(i).getNombre().compareTo(nombre) == 0)
				linea = metro.get(i);
		}
		return linea;
	}

	/**
	 * Método que añade una línea a la red de metro. Lanza una serie de
	 * excepciones si no se cumplen ciertas condiciones.
	 * 
	 * @param linea
	 *            La <code>Linea</code> a añadir.
	 * @throws IllegalArgumentException
	 *             Si la línea es nula.
	 * @since 1.0
	 * @version 1.0
	 */
	public void addLinea(Linea linea) {
		if (linea == null)
			throw new IllegalArgumentException("Linea nula");
		metro.add(linea);
	}

	/**
	 * Método que elimina una línea de la red de metro. Lanza una serie de
	 * excepciones si no se cumplen ciertas condiciones.
	 * 
	 * @param linea
	 *            La <code>Linea</code> a eliminar del metro.
	 * @throws IllegalArgumentException
	 *             Si la línea es nula o no existe.
	 *             
	 * @throws  IllegalStateException
	 *         	   Si el metro no tiene lineas.
	 * @since 1.0
	 * @version 1.0
	 */
	public void removeLinea(Linea linea) {
		if (linea == null)
			throw new IllegalArgumentException("Linea nula");
		if (!metro.contains(linea))
			throw new IllegalArgumentException("Linea inexistente");
		if(this.getLineas().isEmpty())
			throw new IllegalStateException("El metro no tiene lineas");
		if(metro.size() <= 2) 
			throw new IllegalStateException("El metro no puede tener menos de dos líneas");
		metro.remove(linea);
	}

	/**
	 * Método que devuelve una linea a partir del número. Si no se encuentra la
	 * línea devuelve <code>null</code>. Lanza una serie de excepciones si no se
	 * cumplen ciertas condiciones.
	 * 
	 * @param numero
	 *            El número de la línea.
	 * @return linea La <code>Linea</code> que responde a ese número.
	 * @throws IllegalArgumentException
	 *             Si el número está fuera de rango o es negativo.
	 * @throws IllegalStateException
	 *         	   Si el metro no tiene lineas.
	 * @since 1.0
	 * @version 1.0
	 */
	public Linea getLineaNumero(int numero) {
		if(this.getLineas().isEmpty())
			throw new IllegalStateException("El metro no tiene lineas");
		if (numero < 0)
			throw new IllegalArgumentException("Número negativo");
		if (numero > metro.size() - 1)
			throw new IllegalArgumentException("Número fuera de rango");
		for (int i = 0; i < metro.size(); i++) {
			if (metro.get(i).getNumero() == numero)
				return metro.get(i);
		}
		return null;
	}

	/**
	 * Método que dada dos líneas comprueba la correspondencia entre esas
	 * líneas, devolviendo las estaciones que tienen en común. Si no hay
	 * correspondencia devuelve <code>null</code>. Lanza una serie de
	 * excepciones si no se cumplen ciertas condiciones.
	 * 
	 * @param linea1
	 *            Una <code>Linea</code> a comprobar.
	 * @param linea2
	 *            Otra <code>Linea</code> a comprobar.
	 * @return lineasCorrespondencia  Un <code>ArrayList</code> de <code>Estacion</code> con las
	 *         estaciones correspondientes.
	 *         
	 * @throws IllegalStateException
	 *         	   Si el metro no tiene lineas.
	 * 
	 * @since 1.0
	 * @version 1.0
	 */
	public ArrayList<Estacion> getCorrespondenciaLineas(Linea linea1,
			Linea linea2) {
		if(this.getLineas().isEmpty())
			throw new IllegalStateException("El metro no tiene lineas");
		if (linea1 == null)
			throw new IllegalArgumentException("Línea 1 nula");
		if (linea2 == null)
			throw new IllegalArgumentException("Línea 2 nula");
		ArrayList<Estacion> lineasCorrespondencia = new ArrayList<Estacion>();
		for (int i = 0; i < linea1.getEstaciones().size(); i++) {
			for (int j = i + 1; j < linea2.getEstaciones().size(); j++) {
				String nombreLinea1=linea1.getEstaciones().get(i).getNombre();
				String nombreLinea2=linea2.getEstaciones().get(j).getNombre();
				if (nombreLinea1.equals(nombreLinea2))
					lineasCorrespondencia.add(linea1.getEstaciones().get(i));
			}
		}
		if (getLineas().isEmpty())
			return null;
		return lineasCorrespondencia;
	}

	/**
	 * Método que comprueba si las lineas tienen nombre y número únicos,
	 * <code>true</code> si existe unicidad, <code>false</code> si dos o más
	 * lineas tienen el mismo nombre o número. Lanza una serie de excepciones si
	 * no se cumplen ciertas condiciones.
	 * 
	 * @param lineas
	 *            Un <code>ArrayList</code> de <code>Linea</code> para comprobar
	 *            unicidad.
	 * @return unicidad Un <code>boolean</code> que indica si existe unicidad en
	 *         el metro.
	 * @throws IllegalArgumentException
	 *             Si las líneas son nulas o vacías.
	 * @throws IllegalStateException
	 *         	   Si el metro no tiene lineas.
	 * @since 1.0
	 * @version 1.0
	 */
	private boolean compruebaUnicidadLineas(ArrayList<Linea> lineas) {
		if(this.getLineas().isEmpty())
			throw new IllegalStateException("El metro no tiene lineas");
		if (lineas == null)
			throw new IllegalArgumentException("Líneas nula");
		if (lineas.isEmpty())
			throw new IllegalArgumentException("Líneas vacias");
		boolean unicidad = true;
		for (int i = 0; i < lineas.size(); i++) {
			for (int j = i + 1; j < getLineas().size(); j++) {
				if (getLineas().get(i).getNombre()
						.compareTo(getLineas().get(j).getNombre()) == 0)
					unicidad = false;
				if (getLineas().get(i).getNumero() == getLineas().get(j).getNumero())
					unicidad = false;
			}
		}
		return unicidad;
	}

	/**
	 * Método que comprueba si las estaciones tienen nombre único,
	 * <code>true</code> si lo tienen, <code>false</code> si no lo tienen. En el
	 * caso de que dos líneas sean idénticas devuelve <code>false</code>. Lanza
	 * una serie de excepciones si no se cumplen ciertas condiciones.
	 * 
	 * @param lineas
	 *            Un <code>ArrayList</code> de <code>Linea</code> para comprobar
	 *            la unicidad de nombres.
	 * @return unicidad Un <code>boolean</code> que especifica la unicidad.
	 * @throws IllegalArgumentException
	 *             Si las líneas son nulas o están vacías.
	 * @since 1.0
	 * @version 1.0
	 */
	private boolean compruebaUnicidadEstaciones(ArrayList<Linea> lineas) {
		if (lineas == null)
			throw new IllegalArgumentException("Estaciones nula");
		if (lineas.isEmpty())
			throw new IllegalArgumentException("Estaciones vacias");
		boolean unicidad = true;
		for(int i = 0; i < lineas.get(0).getEstaciones().size(); i++){
			for(int j = 1; j < lineas.size()-1; j++){
				if(lineas.get(0).getEstaciones().get(i).getNombre().compareTo(lineas.get(j).getEstaciones().get(i).getNombre()) == 0)
					unicidad = false;
			}
		}
		if(lineas.size() != 1){
			ArrayList<Linea> lineasAux = new ArrayList<Linea>();
			for(int i = 1; i < lineas.size()-1; i++){
				lineasAux.add(lineas.get(i));
			}
			compruebaUnicidadEstaciones(lineasAux);
		}
		return unicidad;
	}
}