/**
 * 
 * @author Daniel Alderete
 * @author Guillermo Pastor
 *
 */
package grupo11_TDS;

/**
 * Clase que representa a las estaciones del metro.
 *
 * @since 1.0
 * @version 1.0
 */
public class Estacion {

	/**
	 * Atributo que representa el nombre de la estación.
	 */
	private String nombre;

	/**
	 * Constructor, necesita el nombre a dar a la estación. Lanza una serie de
	 * excepciones si no se cumplen ciertas condiciones.
	 * 
	 * @param nombre
	 *            El nombre de la estacion
	 * @throws IllegalArgumentException
	 *             Si el nombre está vacío o es null.
	 * 
	 * @since 1.0
	 * @version 1.0
	 */
	public Estacion(String nombre) {
		if (nombre == null)
			throw new IllegalArgumentException("Nombre nulo");
		if (nombre.isEmpty())
			throw new IllegalArgumentException("Nombre no válido");
		this.nombre = nombre;
	}

	/**
	 * Método que devuelve el nombre de la estación.
	 * 
	 * @return nombre El nombre de la estación
	 * 
	 * @since 1.0
	 * @version 1.0
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Método que cambia el nombre de la estación, necesita el nuevo nombre y
	 * lanza una serie de excepciones si no se cumplen ciertas ondiciones.
	 * 
	 * @param nombre El nuevo nombre de la estación.
	 * 
	 * @throws IllegalArgumentException Si el nombre está vacío o es nulo.
	 * 
	 * @since 1.0
	 * @version 1.0
	 */
	public void setNombre(String nombre) {
		if (nombre == null)
			throw new IllegalArgumentException("Nombre nulo");
		if (nombre.isEmpty())
			throw new IllegalArgumentException("Nombre no válido");
		this.nombre = nombre;
	}
}