/**
 * 
 * @author Daniel Alderete
 * @author Guillermo Pastor
 *
 */
package grupo11_TDS;

import static java.lang.Math.*;
import java.util.ArrayList;

/**
 * Clase que representa a las líneas dentro del metro.
 * 
 * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/lang/Math.html">
 *      Math </a>
 * @see <a
 *      href="https://docs.oracle.com/javase/7/docs/api/java/util/ArrayList.html">
 *      ArrayList </a>
 * @see Estacion
 * @since 1.0
 * @version 1.0
 */
public class Linea {

	/**
	 * Nombre de la línea.
	 */
	private String nombre;

	/**
	 * Número de la línea.
	 */
	private int numero;

	/**
	 * Un <code>Arraylist</code> de <code>Estacion</code> con las estaciones que
	 * forman la línea.
	 */
	private ArrayList<Estacion> estaciones;

	/**
	 * Constructor, necesita conocer el nombre, el número y las estaciones que
	 * forman la línea. Lanza una serie de excepciones si no se cumplen ciertas
	 * condiciones.
	 * 
	 * @param nombre
	 *            El nombre de la línea.
	 * @param numero
	 *            El número de la línea.
	 * @param estaciones
	 *            Un <code>Arraylist</code> de <code>Estacion</code> con las
	 *            estaciones que forman la línea.
	 * 
	 * @throws IllegalArgumentException
	 *             Si el nombre está vacío o nulo, si el número es negativo, si
	 *             las estaciones están vacías, si las estaciones tienen el
	 *             mismo nombre o si sólo hay una única estación.
	 * 
	 * @since 1.0
	 * @version 1.0
	 */
	public Linea(String nombre, int numero, ArrayList<Estacion> estaciones) {
		if (nombre == null)
			throw new IllegalArgumentException("Nombre nulo");
		if (nombre.isEmpty())
			throw new IllegalArgumentException("Nombre vacío");
		if (numero < 0)
			throw new IllegalArgumentException("Numero negativo");
		if (estaciones == null)
			throw new IllegalArgumentException("Estaciones nulas");
		if (estaciones.isEmpty())
			throw new IllegalArgumentException("Estaciones vacia");
		if (estaciones.size() == 1)
			throw new IllegalArgumentException("Una única estación");
		if (compruebaNombreEstacionesArray(estaciones))
			throw new IllegalArgumentException("Estaciones con el mismo nombre");
		this.nombre = nombre;
		this.numero = numero;
		this.estaciones = estaciones;
	}

	/**
	 * Método que devuelve el nombre de la línea.
	 * 
	 * @return nombre El nombre de la línea
	 * 
	 * @since 1.0
	 * @version 1.0
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Método que devuelve el número de la línea.
	 * 
	 * @return numero El número de la línea.
	 * 
	 * @since 1.0
	 * @version 1.0
	 */
	public int getNumero() {
		return numero;
	}
	
	/**
	 * Método que devuelve el nombre de la línea.
	 * 
	 * @return nombre El nombre de la línea
	 * 
	 * @since 1.0
	 * @version 1.0
	 */
	public String setNombre(String nombre) {
		if(nombre.isEmpty)
			throws new IllegalArgumentException("Nombre no puede ser vacio");
		return nombre;
	}

	/**
	 * Método que devuelve el número de la línea.
	 * 
	 * @return numero El número de la línea.
	 * 
	 * @since 1.0
	 * @version 1.0
	 */
	public int setNumero(int numero) {
		if(numero<0)
			throws new IllegalArgumentException("No se permiten numeros negativos")
		return numero;
	}

	/**
	 * Método que devuelve la estación inicial.
	 * 
	 * @return Estacion La <code>Estacion</code> incial de la línea.
	 * 
	 * @throws IllegalStateException
	 *             Si la linea no tiene estaciones.
	 * @since 1.0
	 * @version 1.0
	 */
	public Estacion getEstacionInicial() {
		if (this.getEstaciones().isEmpty())
			throw new IllegalStateException("la línea esta vacia");
		return estaciones.get(0);
	}

	/**
	 * Método que devuelve la estación final de la línea.
	 * 
	 * @return Estacion La <code>Estacion</code> final de la línea.
	 * 
	 * @throws IllegalStateException Si la linea no tiene estaciones.
	 * 
	 * @since 1.0
	 * @version 1.0
	 */
	public Estacion getEstacionFinal() {
		if(this.getEstaciones().isEmpty())
			throw new IllegalStateException("la linea esta vacia");
		return estaciones.get(estaciones.size() - 1);
	}

	/**
	 * Método que devuelve un <code>ArrayList</code> de <code>Estacion</code>
	 * con las estaciones que forman la línea.
	 * 
	 * @return estaciones Las estaciones que forman la línea.
	 * 
	 * @throws IllegalStateException Si la linea no tiene estaciones.
	 * 
	 * @since 1.0
	 * @version 1.0
	 */
	public ArrayList<Estacion> getEstaciones() {
		if(this.getEstaciones().isEmpty())
			throw new IllegalStateException("la linea esta vacia");
		return estaciones;
	}

	/**
	 * Método que devuelve las estaciones de de la línea en orden inverso.
	 * 
	 * @return estaciones Un <code>ArrayList</code> de <code>Estacion</code> con
	 *         las estaciones en orden inverso.
	 * 
	 * @throws IllegalStateException Si la linea no tiene estaciones.
	 * @since 1.0
	 * @version 1.0
	 */
	public ArrayList<Estacion> getEstacionesInverso() {
		if(this.getEstaciones().isEmpty())
			throw new IllegalStateException("la linea esta vacia");
		ArrayList<Estacion> inverso = new ArrayList<Estacion>();
		for (int i = estaciones.size() - 1, j = 0; i > 0; i--, j++) {
			try {
				inverso.set(j, estaciones.get(i));
			} catch (IndexOutOfBoundsException e) {
				inverso.add(estaciones.get(i));
			}
		}
		return inverso;
	}

	/**
	 * Método que añade una <code>Estación</code> en la posición indicada. Lanza
	 * una serie de excepciones si no se cumplen ciertas condiciones.
	 * 
	 * @param estacion
	 *            La <code>Estación</code> a añadir.
	 * @param posicion
	 *            La posición donde se quiere añadir
	 * 
	 * @throws IndexOutOfBoundsException
	 *             Si se quiere añadir en la primera o última posición o fuera
	 *             de rango.
	 * @throws IllegalArgumentException
	 *             Si la estación tiene el mismo nombre que otra de la línea.
	 * 
	 * @since 1.0
	 * @version 1.0
	 */
	public void addEstacion(Estacion estacion, int posicion) {
		if (posicion == 0)
			throw new IndexOutOfBoundsException(
					"No se puede añadir en la primera posición");
		if (posicion == estaciones.size() - 1)
			throw new IndexOutOfBoundsException(
					"No se puede añadir en la última posición");
		if (compruebaNombreEstacion(estacion))
			throw new IllegalArgumentException("Estaciones con el mismo nombre");
		if (posicion < 0 || posicion >= estaciones.size())
			throw new IndexOutOfBoundsException("Fuera de rango");
		estaciones.add(posicion, estacion);
	}

	/**
	 * Método que elimina una estación de la línea. Lanza una serie de
	 * excepciones si no se cumplen ciertas condiciones.
	 * 
	 * @param estacion
	 *            La <code>Estación</code> a eliminar
	 * 
	 * @throws IllegalArgumentException
	 *             Si la estación es nula o no existe.
	 * 
	 * @throws IllegalStateException Si la linea no tiene estaciones.
	 * @since 1.0
	 * @version 1.0
	 */
	public void removeEstacion(Estacion estacion) {
		if(this.getEstaciones().isEmpty())
			throw new IllegalStateException("la linea esta vacia");
		if (estacion == null)
			throw new IllegalArgumentException("Estacion nula");
		if (!compruebaNombreEstacion(estacion))
			throw new IllegalArgumentException("Estacion no existente");
		estaciones.remove(estacion);
	}

	/**
	 * Método que cuenta el número de estaciones entre dos estaciones dadas.
	 * Lanza una serie de excepciones si no se cumplen ciertas condiciones.
	 * 
	 * @param inicial
	 *            La <code>Estación</code> a partir de la cual se quiere contar.
	 * @param ultima
	 *            La <code>Estación</code> donde se quiere terminar la cuenta.
	 * @return distancia El número de estaciones entre esas dos estaciones.
	 * 
	 * @throws IllegalArgumentException
	 *             Si alguna de las estaciones es nula o no existe.
	 * 
	 * @throws IllegalStateException Si la linea no tiene estaciones.
	 * @since 1.0
	 * @version 1.0
	 */
	public int separacionEstaciones(Estacion inicial, Estacion ultima) {
		if(this.getEstaciones().isEmpty())
			throw new IllegalStateException("la linea esta vacia");
		if (inicial == null)
			throw new IllegalArgumentException("Estacion inicial nula");
		if (ultima == null)
			throw new IllegalArgumentException("Estacion final nula");
		if (!compruebaNombreEstacion(inicial))
			throw new IllegalArgumentException("Estacion inicial no existe");
		if (!compruebaNombreEstacion(ultima))
			throw new IllegalArgumentException("Estacion final no existe");
		int distancia = abs(estaciones.indexOf(inicial)
				- estaciones.indexOf(ultima)) - 1;
		if (distancia == -1)
			return 0;
		else
			return distancia;
	}

	/**
	 * Método que comprueba si la línea conecta dos estaciones dadas,
	 * <code>true</code> si lo hace, <code>false</code> si no. Lanza una serie
	 * de excepciones si no se cumplen ciertas condiciones.
	 * 
	 * @param estacion1
	 *            La <code>Estación</code> primera a comprobar.
	 * @param estacion2
	 *            La <code>Estación</code> segunda a comprbar.
	 * @return conectadas Un <code>boolean</code> diciendo si están conectadas o
	 *         no.
	 * 
	 * @throws IllegalArgumentException
	 *             Si alguna de las estaciones es nula o es la misma estación.
	 * @throws IllegalStateException Si la linea no tiene estaciones.
	 * @since 1.0
	 * @version 1.0
	 */
	public boolean compruebaConexionEstaciones(Estacion estacion1,
			Estacion estacion2) {
		if(this.getEstaciones().isEmpty())
			throw new IllegalStateException("la linea esta vacia");
		if (estacion1 == null)
			throw new IllegalArgumentException("Estacion inicial nula");
		if (estacion2 == null)
			throw new IllegalArgumentException("Estacion final nula");
		if (estacion1.equals(estacion2))
			throw new IllegalArgumentException(
					"No se puede comprobar la misma estacion");
		return compruebaNombreEstacion(estacion1)
				& compruebaNombreEstacion(estacion2);
	}

	/**
	 * Método que comprueba si una línea tiene una estación, <code>true</code>
	 * si la tiene, <code>false</code> si no. Lanza una serie de excepciones si
	 * no se cumplen ciertas condiciones.
	 * 
	 * @param estacion
	 *            La <code>Estación</code> a comprobar.
	 * @return pertenencia Un <code>boolean</code> que dice si la estación
	 *         pertenece a la línea o no.
	 * 
	 * @throws IllegalArgumentException
	 *             Si la estación es nula
	 * @throws IllegalStateException Si la linea no tiene estaciones.
	 * @since 1.0
	 * @version 1.0
	 */
	public boolean hasEstacion(Estacion estacion) {
		if(this.getEstaciones().isEmpty())
			throw new IllegalStateException("la linea esta vacia");
		if (estacion == null)
			throw new IllegalArgumentException("Estacion nula");
		return compruebaNombreEstacion(estacion);
	}

	/**
	 * Método que comprueba si una estación tiene el mismo nombre que las
	 * existentes, <code>true</code> si lo tiene, <code>false</code> si no.
	 * Lanza una serie de excepciones si no se cumplen ciertas condiciones.
	 * 
	 * @param estacion
	 *            La <code>Estación</code> a comprobar.
	 * @return mismoNombre Un <code>boolean</code> que dice si la estación tiene
	 *         el mismo nombre que otras estaciones de la línea.
	 * 
	 * @throws IllegalArgumentException
	 *             Si la estación es nula
	 * @since 1.0
	 * @version 1.0
	 */
	public boolean compruebaNombreEstacion(Estacion nombreEstacion) {
		if (nombreEstacion == null)
			throw new IllegalArgumentException("Estacion nula");
		boolean mismoNombre = false;
		for (int i = 0; i < estaciones.size(); i++) {
				try{
					estaciones.get(i).getNombre();
				}catch(IndexOutOfBoundsException e){
					System.err.println("No hay estaciones para comparar");
				}
				if(nombreEstacion.equals(nombreEstacion.getNombre())) mismoNombre = true;
		}
		return mismoNombre;
	}

	/**
	 * Método que comprueba si dentro de un <code>ArrayList</code> de estaciones
	 * hay alguna con el mismo nombre, <code>true</code> si lo tiene,
	 * <code>false</code> si no lo tiene. Lanza una serie de excepciones si no
	 * se cumplen ciertas condiciones.
	 * 
	 * @param estaciones
	 *            Un <code>ArrayList</code> de <code>Estación</code>.
	 * @return mismoNombre Un <code>boolean</code> que dice si alguna estación
	 *         tiene el mismo nombre que otra.
	 * 
	 * @throws IllegalArgumentException
	 *             Si el <code>ArrayList</code> está vacío o no.
	 * @throws IllegalStateException Si la linea no tiene estaciones.
	 * @since 1.0
	 * @version 1.0
	 */
	public boolean compruebaNombreEstacionesArray(ArrayList<Estacion> listaEstaciones) {
		if(this.getEstaciones().isEmpty())
			throw new IllegalStateException("la linea esta vacia");
		if (estaciones == null)
			throw new IllegalArgumentException("Estaciones nula");
		if (estaciones.isEmpty())
			throw new IllegalArgumentException("Estaciones vacias");
		boolean existe=false;
		for(int i=0;i<listaEstaciones.size();i++){
			if(compruebaNombreEstacion(listaEstaciones.get(i))) existe=true;
		}
		return existe;
	}
}