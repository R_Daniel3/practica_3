/**
 * 
 * @author Daniel Alderete
 * @author Guillermo Pastor
 *
 */
package grupo11_TDS;

import static org.junit.Assert.*;

import org.junit.Before;

import org.junit.Test;

import grupo11_TDS.*;

import java.util.ArrayList;

public class MetroTests {
    
    @Mock
	private Linea linea1;
	
	@Mock
	private Linea linea2;
	
	@Mock
	private ELinea linea3;
	
	@Mock
	private Linea linea4;
	
	@Mock
	private Linea linea5;
	
	@Mock
	private Linea linea6;
	
	@Mock
	private Linea linea7;
	
	@Mock 
	private Linea lineafalsa;
	
	@Mock 
	private Estacion estacion1;
	
	@Mock 
	private Estacion estacion2;
	
	@Mock 
	private Estacion estacion3;
	
	@Mock 
	private Estacion estacion4;
	
	@Mock 
	private Estacion estacion5;
	
	@Mock 
	private Estacion estacion6;
	
	@Mock 
	private Estacion estacion7;
	
	@Mock 
	private Estacion estacion8;
	
	@Mock 
	private Estacion estacion9;
	
	@Mock 
	private Estacion estacion10;
	
	@Mock 
	private Estacion estacion11;
	
	@Mock 
	private Estacion estacion12;
	
	@Mock 
	private Estacion estacion13;
	
	@Mock 
	private Estacion estacion14;
	
	@Mock 
	private Estacion estacion17;
	
	@Mock 
	private Estacion estacion16;
	
	@Mock
	private Estacion estacion18;
	
	@Mock
	private Estacion estacion19;
	
	@Mock
	private Estacion estacionDePrueba;

	private ArrayList<Linea> lineas;
	private ArrayList<Linea> dosLineas;
	private ArrayList<Linea> unaLinea;
	private ArrayList<Linea> lineasRepetidas;
	private ArrayList<Linea> lineasVacias;
	private ArrayList<Linea> lineasEstacionesRepetidas;
	private ArrayList<Estacion> estaciones1;
	private ArrayList<Estacion> estaciones2;
	private ArrayList<Estacion> estaciones3;
	private ArrayList<Estacion> estaciones4;
	private ArrayList<Estacion> estaciones6;
	private Metro metro;
	private Metro metroSinLineas;

	@Before
	public void setUp() {
		// Metro funcional, con correspondencias
		// Creación de distintos casos de líneas
		lineas = new ArrayList<Linea>();
		dosLineas = new ArrayList<Linea>();
		unaLinea = new ArrayList<Linea>();
		lineasRepetidas = new ArrayList<Linea>();
		lineasEstacionesRepetidas = new ArrayList<Linea>();
		// Creación de los arrays de estaciones
		estaciones1 = new ArrayList<Estacion>();
		estaciones2 = new ArrayList<Estacion>();
		estaciones3 = new ArrayList<Estacion>();
		estaciones4 = new ArrayList<Estacion>();
		estaciones6 = new ArrayList<Estacion>();
		//Creacion de estaciones
		estacion1= createMock(Estacion.class);
		estacion2= createMock(Estacion.class);
		estacion3= createMock(Estacion.class);
		estacion4= createMock(Estacion.class);
		estacion5= createMock(Estacion.class);
		estacion6= createMock(Estacion.class);
		estacion7= createMock(Estacion.class);
		estacion8= createMock(Estacion.class);
		estacion9= createMock(Estacion.class);
		estacion10= createMock(Estacion.class);
		estacion11= createMock(Estacion.class);
		estacion12= createMock(Estacion.class);
		estacion13= createMock(Estacion.class);
		estacion14= createMock(Estacion.class);
		estacion16= createMock(Estacion.class);
		estacion17= createMock(Estacion.class);
		estacion18= createMock(Estacion.class);
		estacion19= createMock(Estacion.class);
		estacionDePrueba= createMock(Estacion.class);
		
		// Estaciones 1
		estaciones1.add(estacion1); //new Estacion("Preobrazhenskaya Ploshchad"));
		estaciones1.add(estacion2); //new Estacion("Vorobyovy Gory"));
		estaciones1.add(estacion3); //new Estacion("Sokolniki"));
		estaciones1.add(estacion4); //new Estacion("Park Kultury"));
		// Estaciones 2
		estaciones2.add(estacion5); //new Estacion("Kashirskaya"));
		estaciones2.add(estacion6); //new Estacion("Tverskaya"));
		estaciones2.add(estacion7); //new Estacion("Kakhovskaya"));
		estaciones2.add(estacion8); //new Estacion("Paveletskaya"));
		// Estaciones 3
		estaciones3.add(estacion9); //new Estacion("Pervomayskaya"));
		estaciones3.add(estacion10); //new Estacion("Park Pobedy"));
		estaciones3.add(estacion11); //new Estacion("Aleksandrovsky Sad"));
		estaciones3.add(estacion12); //new Estacion("Pionerskaya"));
		// Estaciones 4
		estaciones4.add(estacion12); //new Estacion("Pionerskaya"));
		estaciones4.add(estacion11); //new Estacion("Aleksandrovsky Sad"));
		estaciones4.add(estacion13); //new Estacion("Kiyevskaya"));
		estaciones4.add(estacion14); //new Estacion("Smolenskaya"));
		// Estaciones 6
		estaciones6.add(estacion16); //new Estacion("VDNKH"));
		estaciones6.add(estacion17); //new Estacion("Oktyabrskaya"));
		estaciones6.add(estacion18); //new Estacion("Novye Cheryomushki"));
		estaciones6.add(estacion19); //new Estacion("Kitay-gorod"));
		
		
		
		// Creación de las líneas
		linea1 = createMock(Linea.class);
		linea2 = createMock(Linea.class);      //new Linea("Zamoskvoretskaya", 2, estaciones2);
		linea3 = createMock(Linea.class); 	   //new Linea("Arbatsko-Pokrovskaya", 3, estaciones3);
		linea4 = createMock(Linea.class); 	  //new Linea("Filyovskaya", 4, estaciones4);
		linea5 = createMock(Linea.class); 	  //new Linea("Koltsevaya", 5, estaciones1);
		linea6 = createMock(Linea.class);     //new Linea("Kaluzhsko-Rizhskaya", 6, estaciones6);
		lineaFalsa = createMock(Linea.class); //new Linea("Falsa", 7, estaciones1);
		// Asignar las líneas a su array
		lineas.add(linea1);
		lineas.add(linea2);
		lineas.add(linea3);
		lineas.add(linea4);
		// Casos especiales de líneas
		dosLineas.add(linea1);
		dosLineas.add(linea4);
		unaLinea.add(linea1);
		lineasRepetidas.add(linea1);
		lineasRepetidas.add(linea1);
		lineasEstacionesRepetidas.add(linea1);
		lineasEstacionesRepetidas.add(linea5);
		
		//Creacion del metro
		metro= new Metro(lineas);
		
	}



	/*
	 * getLineas: clases válidas
	 */
	@Test
	public void testGetLineas() {
		expect(linea1.getEstaciones().andReturn(estaciones1));
		replay(linea1);
		expect(linea2.getEstaciones().andReturn(estaciones2));
		replay(linea2);
		expect(linea3.getEstaciones().andReturn(estaciones3));
		replay(linea3);
		expect(linea4.getEstaciones().andReturn(estaciones4));
		replay(linea4);
		assertEquals(metro.getLineas(), lineas);
	}
	

	/*
	 * getlineaNombre: clases válidas
	 */
	@Test
	public void testGetLineaNombreValido() {
		expect(linea1.getNombre()).andReturn("Sokolnicheskaya");
		replay(linea1);
		assertEquals(metro.getLineaNombre("Sokolnicheskaya"), linea1);
	}

	/*
	 * getLineaNombre: clases NO válidas
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetLineaNombreNulo() {
		expect(linea1.getNombre()).andReturn("Sokolnicheskaya");
		replay(linea1);
		assertEquals(metro.getLineaNombre(null),linea1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetLineaNombreVacio() {
		expect(linea2.getNombre()).andReturn("Vorobyovy");
		replay(linea2);
		assertEquals(metro.getLineaNombre(""),linea2);
	}
	
	@Test(expected = IllegalStateException.class)
	public void testGetLineaNombreSinLineas(){
		expect(linea1.getNombre()).andReturn("Sokolnicheskaya");
		replay(linea1);
		assertEquals(metroSinLineas.getLineaNombre("Sokolnicheskaya"),linea1);
	}

	/*
	 * getLineaNumero: clases válidas
	 */
	@Test
	public void getLineaNumeroValido() {
		expect(linea1.getNumero()).andReturn(1);
		replay(linea1);
		assertEquals(metro.getLineaNumero(1), lineas.get(0));
	}

	/*
	 * getLineaNumero: clases NO válidas
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetLineaNumeroNegativo() {
		expect(linea1.getNumero()).andReturn(1);
		replay(linea1);
		expect(linea2.getNumero()).andReturn(2);
		replay(linea2);
		expect(linea3.getNumero()).andReturn(3);
		replay(linea3);
		expect(linea4.getNumero()).andReturn(4);
		replay(linea4);
		metro.getLineaNumero(-1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetLineaNumeroFueraDeRango() {
		expect(linea1.getNumero()).andReturn(1);
		replay(linea1);
		expect(linea2.getNumero()).andReturn(2);
		replay(linea2);
		expect(linea3.getNumero()).andReturn(3);
		replay(linea3);
		expect(linea4.getNumero()).andReturn(4);
		replay(linea4);
		metro.getLineaNumero(lineas.size());
	}

	/*
	 * addLinea: clases válidas
	 */
	@Test
	public void testAddLineaValido() {
		expect(linea6.getNombre()).andReturn("Kaluzhsko-Rizhskaya");
		replay(linea6);
		metro.addLinea(linea6);
		assertEquals(metro.getLineaNombre("Kaluzhsko-Rizhskaya"), linea6);
	}
	
	/*
	 * addLinea: clases No válidas
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testAddLineaNula() {
		expect(linea6).andReturn(null);
		replay(linea6);
		metro.addLinea(linea6);
	}


	/*
	 * getLineasPorEstacion: clases válidas
	 */
	@Test
	public void testGetLineasPorEstacion() {
		expect(estacionDePrueba.getNombre()).andReturn("Tverskaya");
		replay(estacionDePrueba);
		expect(linea2.hasEstacion(estacionDePrueba)).andReturn(true);
		replay(linea2);
		expect(linea1.hasEstacion(estacionDePrueba)).andReturn(false);
		replay(linea1);
		expect(linea3.hasEstacion(estacionDePrueba)).andReturn(false);
		replay(linea3);
		expect(linea4.hasEstacion(estacionDePrueba)).andReturn(false);
		replay(linea4);
		assertEquals(metro.getLineasPorEstacion(estacionDePrueba),
				linea2);
	}

	@Test
	public void testGetLineasPorEstacionInexistente() {
		expect(estacionDePrueba.getNombre()).andReturn("Falsa");
		expect(linea2.hasEstacion(estacionDePrueba)).andReturn(false);
		replay(linea2);
		expect(linea1.hasEstacion(estacionDePrueba)).andReturn(false);
		replay(linea1);
		expect(linea3.hasEstacion(estacionDePrueba)).andReturn(false);
		replay(linea3);
		expect(linea4.hasEstacion(estacionDePrueba)).andReturn(false);
		replay(linea4);
		assertEquals(metro.getLineasPorEstacion(estacionDePrueba), null);
	}

	/*
	 * getLineasPorEstacion: clases NO válidas
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetLineasPorEstacionNula() {
		estacionDePrueba=createMock(Estacion.class);
		expect(estacionDePrueba).andReturn(null);
		replay(estacionDePrueba);
		assertEquals(metro.getLineasPorEstacion(estacionDePrueba), linea2);
	}

	@Test(expected = IllegalStateException.class)
	public void testGetLineasPorEstacionSinLineas(){
		estacionDePrueba=createMock(Estacion.class);
		expect(linea2.hasEstacion(estacionDePrueba)).andReturn(true);
		replay(linea2);
		assertEquals(metroSinLineas.getLineasPorEstacion(estacionDePrueba), linea2);
	}
	

	/*
	 * getCorrespondenciaLineas: clases válidas 
	 */
	@Test
	public void testGetCorrespondenciaLineas() {
		//Comportamiento de linea 2
		expect(linea2.getEstaciones()).andReturn(estaciones2);
		replay(linea2);
		//Comportamiento de las estaciones de linea 2
		expect(estacion5.getNombre()).andReturn("Kashirskaya");
		replay(estacion5);
		expect(estacion6.getNombre()).andReturn("Tverskaya");
		replay(estacion6);
		expect(estacion7.getNombre()).andReturn("Kakhovskaya");
		replay(estacion7);
		expect(estacion8.getNombre()).andReturn("Paveletskaya");
		replay(estacion8);
		//Comportamiento de linea 3
		expect(linea3.getEstaciones()).andReturn(estaciones3);
		replay(linea3);
		//Comportamiento de las estaciones de linea 3
		expect(estacion9.getNombre()).andReturn("Pervomayskaya");
		replay(estacion9);
		expect(estacion10.getNombre()).andReturn("Park Pobedy");
		replay(estacion10);
		expect(estacion11.getNombre()).andReturn("Aleksandrovsky Sad");
		replay(estacion11);
		expect(estacion12.getNombre()).andReturn("Pionerskaya");
		replay(estacion12);
		assertEquals(
				metro.getCorrespondenciaLineas(lineas.get(2), lineas.get(3)),
				lineas.get(2).getEstaciones().get(3));
	}

	@Test
	public void testGetCorrespondenciaLineasInexistente() {
		//Comportamiento de linea 1
		expect(linea1.getEstaciones()).andReturn(estaciones1);
		replay(linea1);
		//Comportamiento de las estaciones de linea 1
		expect(estacion1.getNombre()).andReturn("Preobrazhenskaya Ploshchad");
		replay(estacion1);
		expect(estacion2.getNombre()).andReturn("Vorobyovy Gory");
		replay(estacion2);
		expect(estacion3.getNombre()).andReturn("Sokolniki");
		replay(estacion3);
		expect(estacion4.getNombre()).andReturn("Park Kultury");
		replay(estacion4);
		//Comportamiento de linea 3
		expect(linea3.getEstaciones()).andReturn(estaciones3);
		replay(linea3);
		//Comportamiento de las estaciones de linea 3
		expect(estacion9.getNombre()).andReturn("Pervomayskaya");
		replay(estacion9);
		expect(estacion10.getNombre()).andReturn("Park Pobedy");
		replay(estacion10);
		expect(estacion11.getNombre()).andReturn("Aleksandrovsky Sad");
		replay(estacion11);
		expect(estacion12.getNombre()).andReturn("Pionerskaya");
		replay(estacion12);
		assertEquals(
				metro.getCorrespondenciaLineas(lineas.get(1), lineas.get(3)),
				null);
	}

	/*
	 * getCorrespondenciaLineas: clases NO válidas
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetCorrespondenciaLineasLinea1Nula() {
		//Comportamiento de linea 1
		expect(linea1.getEstaciones()).andReturn(estaciones1);
		replay(linea1);
		//Comportamiento de las estaciones de linea 1
		expect(estacion1.getNombre()).andReturn("Preobrazhenskaya Ploshchad");
		replay(estacion1);
		expect(estacion2.getNombre()).andReturn("Vorobyovy Gory");
		replay(estacion2);
		expect(estacion3.getNombre()).andReturn("Sokolniki");
		replay(estacion3);
		expect(estacion4.getNombre()).andReturn("Park Kultury");
		replay(estacion4);
		
		metro.getCorrespondenciaLineas(null, lineas.get(1));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetCorrespondenciaLineasLinea2Nula() {
		//Comportamiento de linea 1
		expect(linea1.getEstaciones()).andReturn(estaciones1);
		replay(linea1);
		//Comportamiento de las estaciones de linea 1
		expect(estacion1.getNombre()).andReturn("Preobrazhenskaya Ploshchad");
		replay(estacion1);
		expect(estacion2.getNombre()).andReturn("Vorobyovy Gory");
		replay(estacion2);
		expect(estacion3.getNombre()).andReturn("Sokolniki");
		replay(estacion3);
		expect(estacion4.getNombre()).andReturn("Park Kultury");
		replay(estacion4);
		
		metro.getCorrespondenciaLineas(lineas.get(1), null);
	}
	
}