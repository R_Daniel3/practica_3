/**
 * 
 * @author Daniel Alderete
 * @author Guillermo Pastor
 *
 */
package grupo11_TDS;

import static org.junit.Assert.*;

import org.junit.Before;

import org.junit.Test;

import grupo11_TDS.*;

import java.util.ArrayList;

public class MetroTests {

	private ArrayList<Linea> lineas;
	private ArrayList<Linea> dosLineas;
	private ArrayList<Linea> unaLinea;
	private ArrayList<Linea> lineasRepetidas;
	private ArrayList<Linea> lineasVacias;
	private ArrayList<Linea> lineasEstacionesRepetidas;
	private ArrayList<Estacion> estaciones1;
	private ArrayList<Estacion> estaciones2;
	private ArrayList<Estacion> estaciones3;
	private ArrayList<Estacion> estaciones4;
	private ArrayList<Estacion> estaciones6;
	private Metro metro;
	private Metro metroSinLineas;
	private Linea linea6;
	private Linea lineaFalsa;
	private Estacion estacionDePrueba;

	@Before
	public void setUp() {
		// Metro funcional, con correspondencias
		// Creación de distintos casos de líneas
		lineas = new ArrayList<Linea>();
		dosLineas = new ArrayList<Linea>();
		unaLinea = new ArrayList<Linea>();
		lineasRepetidas = new ArrayList<Linea>();
		lineasEstacionesRepetidas = new ArrayList<Linea>();
		// Creación de los arrays de estaciones
		estaciones1 = new ArrayList<Estacion>();
		estaciones2 = new ArrayList<Estacion>();
		estaciones3 = new ArrayList<Estacion>();
		estaciones4 = new ArrayList<Estacion>();
		estaciones6 = new ArrayList<Estacion>();
		// Estaciones 1
		estaciones1.add(new Estacion("Preobrazhenskaya Ploshchad"));
		estaciones1.add(new Estacion("Vorobyovy Gory"));
		estaciones1.add(new Estacion("Sokolniki"));
		estaciones1.add(new Estacion("Park Kultury"));
		// Estaciones 2
		estaciones2.add(new Estacion("Kashirskaya"));
		estaciones2.add(new Estacion("Tverskaya"));
		estaciones2.add(new Estacion("Kakhovskaya"));
		estaciones2.add(new Estacion("Paveletskaya"));
		// Estaciones 3
		estaciones3.add(new Estacion("Pervomayskaya"));
		estaciones3.add(new Estacion("Park Pobedy"));
		estaciones3.add(new Estacion("Aleksandrovsky Sad"));
		estaciones3.add(new Estacion("Pionerskaya"));
		// Estaciones 4
		estaciones4.add(new Estacion("Pionerskaya"));
		estaciones4.add(new Estacion("Aleksandrovsky Sad"));
		estaciones4.add(new Estacion("Kiyevskaya"));
		estaciones4.add(new Estacion("Smolenskaya"));
		// Estaciones 6
		estaciones6.add(new Estacion("VDNKH"));
		estaciones6.add(new Estacion("Oktyabrskaya"));
		estaciones6.add(new Estacion("Novye Cheryomushki"));
		estaciones6.add(new Estacion("Kitay-gorod"));
		// Creación de las líneas
		Linea linea1 = new Linea("Sokolnicheskaya", 1, estaciones1);
		Linea linea2 = new Linea("Zamoskvoretskaya", 2, estaciones2);
		Linea linea3 = new Linea("Arbatsko-Pokrovskaya", 3, estaciones3);
		Linea linea4 = new Linea("Filyovskaya", 4, estaciones4);
		Linea linea5 = new Linea("Koltsevaya", 5, estaciones1);
		linea6 = new Linea("Kaluzhsko-Rizhskaya", 6, estaciones6);
		lineaFalsa = new Linea("Falsa", 7, estaciones1);
		// Asignar las líneas a su array
		lineas.add(linea1);
		lineas.add(linea2);
		lineas.add(linea3);
		lineas.add(linea4);
		// Casos especiales de líneas
		dosLineas.add(linea1);
		dosLineas.add(linea4);
		unaLinea.add(linea1);
		lineasRepetidas.add(linea1);
		lineasRepetidas.add(linea1);
		lineasEstacionesRepetidas.add(linea1);
		lineasEstacionesRepetidas.add(linea5);
	}

	/*
	 * Constructor: clases válidas
	 */
	@Test
	public void testCrearMetroValido() {
		Metro metro = new Metro(lineas);
		assertEquals(metro.getLineas(), lineas);
	}

	@Test
	public void testCrearMetroDosLineas() {
		Metro metro = new Metro(dosLineas);
		assertEquals(metro.getLineas(), dosLineas);
	}

	/*
	 * Constructor: clases NO válidas
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testCrearMetroUnaLinea() {
		Metro metro = new Metro(unaLinea);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrearMetroLineaNula() {
		Metro metro = new Metro(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrearMetroLineasVacias() {
		ArrayList<Linea> lineas1 = new ArrayList<Linea>();
		Metro metro = new Metro(lineas1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrearMetroLineasRepetidas() {
		Metro metro = new Metro(lineasRepetidas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrearMetroEstacionesRepetidas() {
		Metro metro = new Metro(lineasEstacionesRepetidas);
	}

	/*
	 * setUp para el metro
	 */
	@Before
	public void setUpMetro() {
		metro = new Metro(lineas);
		estacionDePrueba = new Estacion("Tverskaya");
		lineasVacias = new ArrayList<Linea>();
		metroSinLineas = new Metro(lineasVacias);
	}

	/*
	 * getLineas: clases válidas
	 */
	@Test
	public void testGetLineas() {
		assertEquals(metro.getLineas(), lineas);
	}
	
	/*
	 * getLineas: clases NO válidas 
	 */
	@Test(expected = IllegalStateException.class)
	public void testGetLineasSinLineas(){
		metroSinLineas.getLineas();
	}
	

	/*
	 * getlineaNombre: clases válidas
	 */
	@Test
	public void testGetLineaNombreValido() {
		assertEquals(metro.getLineaNombre("Sokolnicheskaya"), lineas.get(0));
	}

	@Test
	public void testGetLineaNombreValidoInexistente() {
		assertEquals(metro.getLineaNombre("Ninguna"), null);
	}

	/*
	 * getLineaNombre: clases NO válidas
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetLineaNombreNulo() {
		metro.getLineaNombre(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetLineaNombreVacio() {
		metro.getLineaNombre("");
	}
	
	@Test(expected = IllegalStateException.class)
	public void testGetLineaNombreSinLineas(){
		metroSinLineas.getLineaNombre("Sokolnicheskaya");
	}

	/*
	 * getLineaNumero: clases válidas
	 */
	@Test
	public void getLineaNumeroValido() {
		assertEquals(metro.getLineaNumero(1), lineas.get(0));
	}

	/*
	 * getLineaNumero: clases NO válidas
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetLineaNumeroNegativo() {
		metro.getLineaNumero(-1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetLineaNumeroFueraDeRango() {
		metro.getLineaNumero(lineas.size());
	}
	
	@Test(expected = IllegalStateException.class)
	public void testGetLineaNumeroSinLineas(){
		metroSinLineas.getLineaNumero(1);
	}

	/*
	 * addLinea: clases válidas
	 */
	@Test
	public void testAddLineaValido() {
		metro.addLinea(linea6);
		assertEquals(metro.getLineaNombre("Kaluzhsko-Rizhskaya"), linea6);
	}

	/*
	 * addLinea: clases No válidas
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testAddLineaNula() {
		metro.addLinea(null);
	}

	/*
	 * removeLinea: clases válidas
	 */
	@Test
	public void testRemoveLinea() {
		metro.removeLinea(lineas.get(0));
		assertEquals(metro.getLineas(), lineas);
	}

	/*
	 * removeLinea: clases NO válidas
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRemoveLineaNula() {
		metro.removeLinea(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testRemoveLineaInexistente() {
		metro.removeLinea(lineaFalsa);
	}
	
	@Test(expected = IllegalStateException.class)
	public void testRemoveLineaSinLineas(){
		metroSinLineas.removeLinea(lineas.get(1));
	}

	/*
	 * getLineasPorEstacion: clases válidas
	 */
	@Test
	public void testGetLineasPorEstacion() {
		assertEquals(metro.getLineasPorEstacion(estacionDePrueba),
				lineas.get(1));
	}

	@Test
	public void testGetLineasPorEstacionInexistente() {
		assertEquals(metro.getLineasPorEstacion(new Estacion("Falsa")), null);
	}

	/*
	 * getLineasPorEstacion: clases NO válidas
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetLineasPorEstacionNula() {
		metro.getLineasPorEstacion(null);
	}
	
	@Test(expected = IllegalStateException.class)
	public void testGetLineasPorEstacionSinLineas(){
		metroSinLineas.getLineasPorEstacion(estacionDePrueba);
	}
	

	/*
	 * getCorrespondenciaLineas: clases válidas 
	 */
	@Test
	public void testGetCorrespondenciaLineas() {
		assertEquals(
				metro.getCorrespondenciaLineas(lineas.get(2), lineas.get(3)),
				lineas.get(2).getEstaciones().get(3));
	}

	@Test
	public void testGetCorrespondenciaLineasInexistente() {
		assertEquals(
				metro.getCorrespondenciaLineas(lineas.get(1), lineas.get(3)),
				null);
	}

	/*
	 * getCorrespondenciaLineas: clases NO válidas
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetCorrespondenciaLineasLinea1Nula() {
		metro.getCorrespondenciaLineas(null, lineas.get(1));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetCorrespondenciaLineasLinea2Nula() {
		metro.getCorrespondenciaLineas(lineas.get(1), null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetCorrespondenciaLineasNulas() {
		metro.getCorrespondenciaLineas(null, null);
	}
	
	@Test(expected = IllegalStateException.class)
	public void testGetCorrespondenciaLineasSinLineas(){
		metroSinLineas.getCorrespondenciaLineas(lineas.get(1), lineas.get(3));
	}
}