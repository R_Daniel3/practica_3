/**
 * 
 * @author Daniel Alderete
 * @author Guillermo Pastor
 *
 */
package grupo11_TDS;

import grupo11_TDS.*;

import static org.junit.Assert.*;

import org.junit.Before;

import org.junit.Test;

import java.util.ArrayList;

/*
 * Clase para realizar las pruebas en secuencia
 */
public class PruebasSecuencia{
    
    /* Metro para realizar las pruebas */
    private Metro metro;
    
    /* Array de Linea para realizar las pruebas */
    private ArrayList<Linea> lineas;
    
    /* Arrays de Estacion para realizar las pruebas */
    private ArrayList<Estacion> estaciones1;
    private ArrayList<Estacion> estaciones2;
    private ArrayList<Estacion> estaciones3;
    private ArrayList<Estacion> estaciones4;
    private ArrayList<Estacion> estaciones5;
    private ArrayList<Estacion> estaciones6;
    
    /* Diferentes Lineas */
    private Linea linea1;
    private Linea linea2;
    private Linea linea3;
    private Linea linea4;
    private Linea linea5;
    private Linea linea6;
    
    @Before
    public void setUp(){
        // Inicializar los distintos campos 
        lineas = new ArrayList<Linea>();
        estaciones1 = new ArrayList<Estacion>();
        estaciones2 = new ArrayList<Estacion>();
        estaciones3 = new ArrayList<Estacion>();
        estaciones4 = new ArrayList<Estacion>();
        estaciones5 = new ArrayList<Estacion>();
        estaciones6 = new ArrayList<Estacion>();
        
        //Estaciones1
        estaciones1.add(new Estacion("Preobrazhenskaya Ploshchad"));
		estaciones1.add(new Estacion("Vorobyovy Gory"));
		estaciones1.add(new Estacion("Sokolniki"));
		estaciones1.add(new Estacion("Park Kultury"));
		// Estaciones 2
		estaciones2.add(new Estacion("Kashirskaya"));
		estaciones2.add(new Estacion("Tverskaya"));
		estaciones2.add(new Estacion("Kakhovskaya"));
		estaciones2.add(new Estacion("Paveletskaya"));
		// Estaciones 3
		estaciones3.add(new Estacion("Pervomayskaya"));
		estaciones3.add(new Estacion("Park Pobedy"));
		estaciones3.add(new Estacion("Aleksandrovsky Sad"));
		estaciones3.add(new Estacion("Pionerskaya"));
		// Estaciones 4
		estaciones4.add(new Estacion("Pionerskaya"));
		estaciones4.add(new Estacion("Aleksandrovsky Sad"));
		estaciones4.add(new Estacion("Kiyevskaya"));
		estaciones4.add(new Estacion("Smolenskaya"));
		//Estaciones5
        estaciones5.add(new Estacion("VDNKH"));
    	estaciones5.add(new Estacion("Oktyabrskaya"));
    	estaciones5.add(new Estacion("Novye Cheryomushki"));
    	estaciones5.add(new Estacion("Kitay-gorod"));
    	//Estaciones6
        estaciones6.add(new Estacion("Tetravoskaya"));
    	estaciones6.add(new Estacion("Belorruskaya"));
    	estaciones6.add(new Estacion("Dobryiniskaya"));
    	estaciones6.add(new Estacion("Prospekt Mira"));
    	
		//Creación de las líneas
		linea1 = new Linea("Sokolnicheskaya", 1, estaciones1);
		linea2 = new Linea("Zamoskvoretskaya", 2, estaciones2);
		linea3 = new Linea("Arbatsko-Pokrovskaya", 3, estaciones3);
		linea4 = new Linea("Filyovskaya", 4, estaciones4);
		linea5 = new Linea("Koltsevaya", 5, estaciones5);
        linea6 = new Linea("Kaluzhsko-Rizhskaya", 6, estaciones6);
        
		// Asignar las líneas a su array
		lineas.add(linea1);
		lineas.add(linea2);
		
		//Creación del metro
		metro = new Metro(lineas);
        
    }
    
    /* Secuencia Correcta */
    @Test
    public void testSecuenciaCorrecta(){
        //Añadir línea
        metro.addLinea(linea5);
        //Borrar línea
        metro.removeLinea(linea5);
        //Añadir dos líneas
        metro.addLinea(linea5);
        metro.addLinea(linea6);
        //Borrar dos línea
        metro.removeLinea(linea5);
        metro.removeLinea(linea6);
        //Consultar líneas
        metro.getLineas();
        //SetMetro
        ArrayList<Linea> lineasPrueba = new ArrayList<Linea>();
        lineasPrueba.add(linea4);
        lineasPrueba.add(linea5);
        metro.setMetro(lineasPrueba);
        //getLineasPorEstacion
        metro.getLineasPorEstacion(estaciones4.get(0));
        //getCorrespondenciaLineas
        metro.getCorrespondenciaLineas(linea4, linea5);
        //getLineaNombre
        metro.getLineaNombre("Filyovskaya");
        //getLineaNumero
        metro.getLineaNumero(4);

    }
    
    /* Secuencia de fallos 1 */
    @Test(expected = IllegalArgumentException.class)
    public void testSecuenciaFallida1(){
        metro.setMetro(null);
    }
    
    /* Secuencia de fallos 2 */
    @Test(expected = IllegalStateException.class)
    public void testSecuenciaFallida2(){
        metro.addLinea(linea3);
        metro.removeLinea(linea2);
        ArrayList<Linea> lineasPrueba = new ArrayList<Linea>();
        lineasPrueba.add(linea4);
        lineasPrueba.add(linea5);
        metro.setMetro(lineasPrueba);
        metro.addLinea(linea6);
        metro.removeLinea(linea4);
        metro.removeLinea(linea5);

    }
    
    /* Secuencia de fallos 3 */
    @Test(expected = IllegalArgumentException.class)
    public void testSecuenciaFallida3(){
        metro.addLinea(linea3);
        metro.removeLinea(linea1);
        metro.getLineasPorEstacion(null);
        
    }
    
    /* Secuencia de fallos 4 */
    @Test(expected = IllegalArgumentException.class)
    public void testSecuenciaFallida4(){
        metro.addLinea(linea4);
        metro.removeLinea(linea1);
        ArrayList<Linea> lineasPrueba = new ArrayList<Linea>();
        lineasPrueba.add(linea3);
        lineasPrueba.add(linea6);
        metro.setMetro(lineasPrueba);
        metro.getCorrespondenciaLineas(null, null);
    }
    
    /* Secuencia de fallos 5 */
    @Test(expected = IllegalArgumentException.class)
    public void testSecuenciaFallida5(){
        metro.addLinea(linea4);
        metro.removeLinea(linea1);
        ArrayList<Linea> lineasPrueba = new ArrayList<Linea>();
        lineasPrueba.add(linea3);
        lineasPrueba.add(linea6);
        metro.setMetro(lineasPrueba);
        metro.getLineaNombre(null);
    }
    
    /* Secuencia de fallos 6 */
    @Test(expected = IllegalArgumentException.class)
    public void testSecuenciaFallida6(){
        metro.addLinea(linea4);
        metro.removeLinea(linea1);
        ArrayList<Linea> lineasPrueba = new ArrayList<Linea>();
        lineasPrueba.add(linea3);
        lineasPrueba.add(linea6);
        metro.setMetro(lineasPrueba);
        metro.getLineaNumero(-1);
    }
}