/**
 * 
 * @author Daniel Alderete
 * @author Guillermo Pastor
 *
 */
package grupo11_TDS;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ EstacionTests.class, LineaTests.class, MetroTests.class })
/*
 * JUnit Test Suite para las clases EstacionTests, LineaTests, MetroTests
 */
public class AllTests {

}
