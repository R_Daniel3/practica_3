/**
 * 
 * @author Daniel Alderete
 * @author Guillermo Pastor
 *
 */
package grupo11_TDS;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.Before;

import grupo11_TDS.*;

import java.util.ArrayList;

import org.easymock.Mock;

import static org.easymock.EasyMock.*;

/*
 * Clase de pruebas en aislamiento para Linea.
 */
public class LineaTestsAislamiento {
	
	@Mock
	private Estacion estacion1;
	
	@Mock
	private Estacion estacion2;
	
	@Mock
	private Estacion estacion3;
	
	@Mock
	private Estacion estacion4;
	
	@Mock
	private Estacion estacion5;
	
	@Mock
	private Estacion estacion7;
	
	@Mock
	private Estacion estacion8;
	
	@Mock
	private Estacion estacion9;
	
	@Mock
	private Estacion estacion10;
	
	@Mock
	private Estacion estacion11;
	
	@Mock
	private Estacion estacion12;
	
	@Mock
	private Estacion estacion13;
	
	@Mock
	private Estacion estacion14;
	
	@Mock
	private Estacion estacion;
	
	@Mock
	private Estacion estacionDePrueba;

	/*
	 * SetUp de las estaciones para crear un array de estaciones válidas
	 */
	private ArrayList<Estacion> estaciones;
	private ArrayList<Estacion> estacionVacia;
	private ArrayList<Estacion> inverso;
	private ArrayList<Estacion> estaciones1;
	private ArrayList<Estacion> estacionesSegundaPosicion;
	private ArrayList<Estacion> estacionesBorrado;
	private ArrayList<Estacion> estacionesPenultimaPosicion;

	@Before
	public void setUp(){
		estaciones = new ArrayList<Estacion>();
		estacion1 = createMock(Estacion.class);
		estacion2 = createMock(Estacion.class);
		estacion3 = createMock(Estacion.class);
		estacion4 = createMock(Estacion.class);
		estacion5 = createMock(Estacion.class);

		estaciones.add(estacion1);
		estaciones.add(estacion2);
		estaciones.add(estacion3);
		estaciones.add(estacion4);
		estaciones.add(estacion5);

		estacionVacia = new ArrayList<Estacion>();
		estacionVacia.add(estacion1);
		estacionVacia.remove(estacion1);
		
		ArrayList<Estacion> inverso = new ArrayList<Estacion>();
		for (int i = estaciones.size()-1, j = 0; i > 0; i--, j++){
			try{
				inverso.set(j, estaciones.get(i));
			}catch(IndexOutOfBoundsException e){
				inverso.add(estaciones.get(i));
			}
		}
		
		ArrayList<Estacion> estaciones1 = new ArrayList<Estacion>();
		for(int i = 0; i < estaciones.size(); i++){
			try{
				estaciones1.set(i, estaciones.get(i));
			}catch(IndexOutOfBoundsException e){
				estaciones1.add(estaciones.get(i));
			}
		}
		
		ArrayList<Estacion> estacionesSegundaPosicion = new ArrayList<Estacion>();
		for(int i = 0; i < estaciones.size(); i++){
			try{
				estacionesSegundaPosicion.set(i, estaciones.get(i));
			}catch(IndexOutOfBoundsException e){
				estacionesSegundaPosicion.add(estaciones.get(i));
			}
		}
		
		ArrayList<Estacion> estacionesPenultimaPosicion = new ArrayList<Estacion>();
		for(int i = 0; i < estaciones.size(); i++){
			try{
				estacionesPenultimaPosicion.set(i, estaciones.get(i));
			}catch(IndexOutOfBoundsException e){
				estacionesPenultimaPosicion.add(estaciones.get(i));
			}
		}
		
		ArrayList<Estacion> estacionesBorrado = new ArrayList<Estacion>();
		for(int i = 0; i < estaciones.size(); i++){
			try{
				estacionesBorrado.set(i, estaciones.get(i));
			}catch(IndexOutOfBoundsException e){
				estacionesBorrado.add(estaciones.get(i));
			}
		}
	}

	/*
	 * Constructor: clases válidas
	 */
	@Test
	public void testCrearLineaValida() {
		Linea linea = new Linea("Roja", 1, estaciones);
		assertEquals(linea.getNombre(), "Roja");
		assertEquals(linea.getNumero(), 1);
		assertEquals(linea.getEstaciones(), estaciones);
	}

	@Test
	public void testCrearLineaValidaCero(){
		Linea linea = new Linea("Roja", 0, estaciones);
		assertEquals(linea.getNombre(), "Roja");
		assertEquals(linea.getNumero(), 0);
		assertEquals(linea.getEstaciones(), estaciones);
	}

	/*
	 * Constructor: clases NO válidas
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testCrearLineaNombreNulo(){
		Linea linea = new Linea(null, 1, estaciones);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testCrearLineaNombreVacio(){
		Linea linea = new Linea("", 1, estaciones);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testCrearLineaNumeroNegativo(){
		Linea linea = new Linea("Filyovskaya", -1, estaciones);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testCrearLineaSinEstaciones(){
		ArrayList<Estacion> estaciones1 = new ArrayList<Estacion>();
		Linea linea = new Linea("Filyovskaya", 1, estaciones1);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testCrearLineaEstacionesNulas(){
		Linea linea = new Linea("Filyovskaya", 1, null);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testCrearLineaUnaEstacion(){
		ArrayList<Estacion> estaciones1 = new ArrayList<Estacion>();
		estacion7 = createMock(Estacion.class);
		estaciones1.add(estacion7);
		Linea linea = new Linea("Filyovskaya", 1, estaciones1);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testCrearLineaEstacionesConMismoNombre(){
		ArrayList<Estacion> estacionesMismoNombre = new ArrayList<Estacion>();
		estacion7 = createMock(Estacion.class);
		estacion8 = createMock(Estacion.class);
		estacion9 = createMock(Estacion.class);
		estacion10 = createMock(Estacion.class);
		estacionesMismoNombre.add(estacion7);
		estacionesMismoNombre.add(estacion8);
		estacionesMismoNombre.add(estacion9);
		estacionesMismoNombre.add(estacion10);
		Linea linea = new Linea("Filyovskaya", 1, estacionesMismoNombre);
	}

	/*
	 * getNombre
	 */
	@Test
	public void testGetNombre(){
		Linea linea = new Linea("Koltsevaya", 2, estaciones);
		assertEquals(linea.getNombre(), "Koltsevaya");
	}

	/*
	 * getNumero
	 */
	@Test
	public void testGetNumero(){
		Linea linea = new Linea("Koltsevaya", 2, estaciones);
		assertEquals(linea.getNumero(), 2);
	}

	/*
	 * getEstacionInicial
	 */
	@Test
	public void testGetEstacionInicial(){
		Linea linea = new Linea("Koltsevaya", 2, estaciones);
		assertEquals(linea.getEstacionInicial(), estaciones.get(0));
	}

	/*
	 * getEstacionFinal
	 */
	@Test
	public void testGetEstacionFinal(){
		Linea linea = new Linea("Koltsevaya", 2, estaciones);
		assertEquals(linea.getEstacionFinal(), estaciones.get(4));
	}

	/*
	 * getEstaciones
	 */
	@Test
	public void testGetEstacioens(){
		Linea linea = new Linea("Koltsevaya", 2, estaciones);
		assertEquals(linea.getEstaciones(), estaciones);
	}

	/*
	 * getInverso Corregir
	 */
	@Test
	public void testGetEstacionesInverso(){
		Linea linea = new Linea("Koltsevaya", 2, estaciones);
		assertEquals(linea.getEstacionesInverso(), inverso);
	}

	/*
	 * addEstacion: Clases válidas
	 */
	@Test
	public void testAddEstacionValido(){
		Linea linea = new Linea("Sokolnicheskaya", 2, estaciones);
		estacion = createMock(Estacion.class);
        estaciones1.add(3, estacion);
		linea.addEstacion(estacion, 3);
		assertEquals(linea.getEstaciones(), estaciones1);
	}

	@Test
	public void testAddEstacionValidoSegundaPosicion(){
		Linea linea = new Linea("Sokolnicheskaya", 2, estaciones);
		estacion = createMock(Estacion.class);
        estacionesSegundaPosicion.add(1, estacion);
		linea.addEstacion(estacion, 1);
		assertEquals(linea.getEstaciones(), estacionesSegundaPosicion);
	}

	@Test
	public void testAddEstacionValidoPenultimaPosicion(){
		Linea linea = new Linea("Sokolnicheskaya", 2, estaciones);
		estacion = createMock(Estacion.class);
        estacionesPenultimaPosicion.add(estaciones.size()-2, estacion);
		linea.addEstacion(estacion, estaciones.size()-2);
		assertEquals(linea.getEstaciones(), estacionesPenultimaPosicion);
	}

	/*
	 * addEstacion: clases NO válidas
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testAddEstacionNula(){
		Linea linea = new Linea("Sokolnicheskaya", 2, estaciones);
		linea.addEstacion(null, 3);
	}

	@Test(expected=IndexOutOfBoundsException.class)
	public void testAddEstacionPrimeraPosicion(){
		Linea linea = new Linea("Sokolnicheskaya", 2, estaciones);
		estacion = createMock(Estacion.class);
		linea.addEstacion(estacion, 0);
	}

	@Test(expected=IndexOutOfBoundsException.class)
	public void testAddEstacionNumeroNegativo(){
		Linea linea = new Linea("Sokolnicheskaya", 2, estaciones);
		estacion = createMock(Estacion.class);
		linea.addEstacion(estacion, -1);
	}

	@Test(expected=IndexOutOfBoundsException.class)
	public void testAddEstacionUltimaPosicion(){
		Linea linea = new Linea("Sokolnicheskaya", 2, estaciones);
		estacion = createMock(Estacion.class);
		linea.addEstacion(estacion, estaciones.size()-1);
	}

	@Test(expected=IndexOutOfBoundsException.class)
	public void testAddEstacionFueraDeRango(){
		Linea linea = new Linea("Sokolnicheskaya", 2, estaciones);
		estacion = createMock(Estacion.class);
		linea.addEstacion(estacion, estaciones.size());
	}

	@Test(expected=IllegalArgumentException.class)
	public void testAddEstacionMismoNombre(){
		Linea linea = new Linea("Sokolnicheskaya", 2, estaciones);
		estacion = createMock(Estacion.class);
		linea.addEstacion(estacion, estaciones.size());
	}

	/*
	 * removeEstacion: clases válidas
	 */
	@Test
	public void testRemoveEstacion(){
		Linea linea = new Linea("Zamoskvoretskaya", 4, estaciones);
		estacion = estaciones.get(3);
		estacionesBorrado.remove(estacion);
		linea.removeEstacion(estacion);
		assertEquals(linea.getEstaciones(), estacionesBorrado);
	}

	/*
	 * removeEstacion: clases NO válidas
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testRemoveEstacionNula(){
		Linea linea = new Linea("Zamoskvoretskaya", 4, estaciones);
		linea.removeEstacion(null);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testRemoveEstacionNoExistente(){
		Linea linea = new Linea("Zamoskvoretskaya", 4, estaciones);
		estacion = createMock(Estacion.class);
		linea.removeEstacion(estacion);
	}
	
	@Test(expected=IllegalStateException.class)
	public void testRemoveLineaSinEstaciones(){
		Linea linea = new Linea("Zamoskvoretskaya", 4, estacionVacia);
		linea.removeEstacion(estacionDePrueba);
	}

	/*
	 * separacionEstaciones: clases válidas
	 */
	@Test
	public void testSeparacionEstaciones(){
		Linea linea = new Linea("Arbatsko-Pokrovskaya", 5, estaciones);
		assertSame(linea.separacionEstaciones(estaciones.get(0), estaciones.get(3)), 2);
	}

	@Test
	public void testSeparacionMismaEstacion(){
		Linea linea = new Linea("Arbatsko-Pokrovskaya", 5, estaciones);
		assertSame(linea.separacionEstaciones(estaciones.get(0), estaciones.get(0)), 0);
	}

	@Test
	public void testSeparacionALaInversa(){
		Linea linea = new Linea("Arbatsko-Pokrovskaya", 5, estaciones);
		assertSame(linea.separacionEstaciones(estaciones.get(3), estaciones.get(0)), 2);
	}

	/*
	 * separacionEstaciones: clases NO válidas
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testSeparacionEstacionInicialNula(){
		Linea linea = new Linea("Arbatsko-Pokrovskaya", 5, estaciones);
		linea.separacionEstaciones(null, estaciones.get(3));
	}

	@Test(expected=IllegalArgumentException.class)
	public void testSeparacionEstacionFinalNula(){
		Linea linea = new Linea("Arbatsko-Pokrovskaya", 5, estaciones);
		linea.separacionEstaciones(estaciones.get(3), null);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testSeparacionEstacionesNulas(){
		Linea linea = new Linea("Arbatsko-Pokrovskaya", 5, estaciones);
		linea.separacionEstaciones(null, null);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testSeparacionEstacionInicialInexistente(){
		Linea linea = new Linea("Arbatsko-Pokrovskaya", 5, estaciones);
		estacion = createMock(Estacion.class);
		linea.separacionEstaciones(estacion, estaciones.get(0));
	}

	@Test(expected=IllegalArgumentException.class)
	public void testSeparacionEstacionFinalInexistente(){
		Linea linea = new Linea("Arbatsko-Pokrovskaya", 5, estaciones);
		estacion = createMock(Estacion.class);
		linea.separacionEstaciones(estaciones.get(0), estacion);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testSeparacionEstacionesInexistentes(){
		Linea linea = new Linea("Arbatsko-Pokrovskaya", 5, estaciones);
		estacion11 = createMock(Estacion.class);
		estacion12 = createMock(Estacion.class);
		linea.separacionEstaciones(estacion11, estacion12);
	}

	/*
	 * compruebaConexionEstaciones: clases válidas
	 */
	@Test
	public void tetsCompruebaConexionEstacionesConectadasValido(){
		Linea linea = new Linea("Tagansko-Krasnopresnenskaya", 6, estaciones);
		assertTrue(linea.compruebaConexionEstaciones(estaciones.get(1), estaciones.get(3)));
	}

	@Test
	public void tetsCompruebaConexionEstacionesNoConectadasValido(){
		Linea linea = new Linea("Tagansko-Krasnopresnenskaya", 6, estaciones);
		estacion13 = createMock(Estacion.class);
		assertFalse(linea.compruebaConexionEstaciones(estaciones.get(1), estacion13));
	}

	/*
	 * compruebaEstaciones: clases NO válidas
	 */
	@Test(expected = IllegalArgumentException.class)
	public void tetsCompruebaConexionEstacionesEstacionInicialNula(){
		Linea linea = new Linea("Tagansko-Krasnopresnenskaya", 6, estaciones);
		linea.compruebaConexionEstaciones(null, estaciones.get(3));
	}

	@Test(expected = IllegalArgumentException.class)
	public void tetsCompruebaConexionEstacionesEstacionFinalNula(){
		Linea linea = new Linea("Tagansko-Krasnopresnenskaya", 6, estaciones);
		linea.compruebaConexionEstaciones(estaciones.get(0), null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void tetsCompruebaConexionEstacionesNulas(){
		Linea linea = new Linea("Tagansko-Krasnopresnenskaya", 6, estaciones);
		linea.compruebaConexionEstaciones(null, null);
	}

	@Test //(expected = IllegalArgumentException.class)
	public void tetsCompruebaConexionEstacionesMismaEstacion(){
		Linea linea = new Linea("Tagansko-Krasnopresnenskaya", 6, estaciones);
		assertFalse(linea.compruebaConexionEstaciones(estaciones.get(1), estaciones.get(1)));
	}

	/*
	 * hasEstacion: clases válidas
	 */
	@Test
	public void testHasEstacionTiene(){
		Linea linea = new Linea("Kalininsko-Solntsevskaya", 7, estaciones);
		assertTrue(linea.hasEstacion(estaciones.get(2)));
	}

	@Test
	public void testHasEstacionNoTiene(){
		Linea linea = new Linea("Kalininsko-Solntsevskaya", 7, estaciones);
		estacion14 = createMock(Estacion.class);
		assertFalse(linea.hasEstacion(estacion14));
	}

	/*
	 * hasEstacion: clases NO válidas
	 */
	@Test(expected = IllegalArgumentException.class)
	public void tetsHasEstacionNula(){
		Linea linea = new Linea("Kalininsko-Solntsevskaya", 7, estaciones);
		linea.hasEstacion(null);
	}

	/*
	 * compruebaNombreEstacion: clases válidas
	 */
	@Test
	public void testCompruebaNombreEstacionTiene(){
		Linea linea = new Linea("Kalininsko-Solntsevskaya", 7, estaciones);
		assertTrue(linea.compruebaNombreEstacion(estaciones.get(2)));
	}

	@Test
	public void testCompruebaNombreEstacionNoTiene(){
		Linea linea = new Linea("Kalininsko-Solntsevskaya", 7, estaciones);
		estacion14 = createMock(Estacion.class);
		assertFalse(linea.compruebaNombreEstacion(estacion14));
	}

	/*
	 * hasEstacion: clases NO válidas
	 */
	@Test(expected = IllegalArgumentException.class)
	public void tetsCompruebaNombreEstacionEstacionNula(){
		Linea linea = new Linea("Kalininsko-Solntsevskaya", 7, estaciones);
		linea.compruebaNombreEstacion(null);
	}

	/*
	 * compruebaNombreEstacionesArray: clases válidas
	 */
	@Test
	public void testCompruebaNombreEstacionesArray(){
		Linea linea = new Linea("Kalininsko-Solntsevskaya", 7, estaciones);
		assertFalse(linea.compruebaNombreEstacionesArray(estaciones));
	}

	@Test
	public void testCompruebaNombreEstacionesArrayEstacionesMismoNombre(){
		estacion7 = createMock(Estacion.class);
		estacion8 = createMock(Estacion.class);
		estacion9 = createMock(Estacion.class);
		estacion10 = createMock(Estacion.class);
		ArrayList<Estacion> estaciones1 = new ArrayList<Estacion>();
		estaciones1.add(estacion7);
		estaciones1.add(estacion8);
		estaciones1.add(estacion9);
		estaciones1.add(estacion10);
		Linea linea = new Linea("Kalininsko-Solntsevskaya", 7, estaciones);
		assertTrue(linea.compruebaNombreEstacionesArray(estaciones1));
	}

	/*
	 * compruebaNombreEstacionesArray: clases NO válidas
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testCompruebaNombreEstacionesArrayNulo(){
		Linea linea = new Linea("Kalininsko-Solntsevskaya", 7, estaciones);
		linea.compruebaNombreEstacionesArray(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCompruebaNombreEstacionesArrayVacio(){
		ArrayList<Estacion> estaciones1 = new ArrayList<Estacion>();
		Linea linea = new Linea("Kalininsko-Solntsevskaya", 7, estaciones1);
		linea.compruebaNombreEstacionesArray(estaciones1);
	}

}