/**
 * 
 * @author Daniel Alderete
 * @author Guillermo Pastor
 *
 */
package grupo11_TDS;

import static org.junit.Assert.*;

import org.junit.Test;

import grupo11_TDS.*;

public class EstacionTests {

	/*
	 * Cosntructor: Test de clases válidas
	 */
	@Test
	public void testCrearEstacion() {
		Estacion estacion = new Estacion("estacion1");
		assertEquals(estacion.getNombre(), "estacion1");
	}
	
	/*
	 * Constructor: Test de clases NO válidas
	 */
	@Test(expected=IllegalArgumentException.class)
	public void tetsCrearEstacionNula(){
		Estacion estacion = new Estacion(null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void tetsCrearEstacionVacia(){
		Estacion estacion = new Estacion("");
	}
	
	/*
	 * getNombre
	 */
	@Test
	public void testGetNombre(){
		Estacion estacion = new Estacion("estacion1");
		String cadena = estacion.getNombre();
		assertEquals(cadena, estacion.getNombre());
	}
	
	/*
	 * setNombre: clases válidas
	 */
	@Test
	public void testSetNombreVálido(){
		Estacion estacion = new Estacion("estacion1");
		estacion.setNombre("nuevaEstacion");
		assertEquals(estacion.getNombre(), "nuevaEstacion");
	}
	
	/*
	 * setNombre: clases NO válidas
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testSetNombreNulo(){
		Estacion estacion = new Estacion("estacion1");
		estacion.setNombre(null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetNombreVacio(){
		Estacion estacion = new Estacion("estacion1");
		estacion.setNombre("");
	}

}